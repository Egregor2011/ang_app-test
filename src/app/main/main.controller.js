(function (){
'use strict';


function MainCtrl(apiRequest){
  var self = this;
  self.numbers = 10;
  self.skip = 0;
  self.regChange = function(){
    if (self.reg){
      self.reg = self.reg.replace(/\s+/, '-');}
  };
  self.req_done = false;
  self.mydata = function () {
    apiRequest.getdata(this.reg, this.numbers, this.skip).then(function (data) {
      self.name = data;
    });
    self.req_done = true;
  }

}


angular.module('angAppTest')
  .controller('MainCtrl', ['apiRequest',MainCtrl])

})();
