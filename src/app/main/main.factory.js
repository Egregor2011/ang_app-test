(function(){

  function apiRequest($http){
    var apiRequest = {};
    apiRequest.getdata =  function (reg, num, skip) {
      return $http.get('http://www.corsproxy.com/maps.yandex.ru/services/search/1.x/search.json?',
        {
          params: {
            "text": reg,
            "results": num,
            "origin": "maps-pager",
            "lang": "ru-RU",
            "skip": skip
          }
        }).
        then(function (result) {
          var i,
            obj = {},
            names =  [],
            links =  [],

            test = result.data.features;

          angular.forEach(test, function (value) {

            angular.forEach(value, function (value, key) {

              if (key === "properties") {

                angular.forEach(value, function (value, key) {

                  if (key === 'name') {
                    names.push(value);
                  }
                  ;
                  if (key === 'CompanyMetaData') {
                    angular.forEach(value, function (value, key) {
                      if (key === 'url') {
                        links.push(value);
                      }
                    })
                  }
                });
              }
              ;
            });
          });
          for (i = 0; i < names.length; ++i){
            obj[names[i]] = links[i];
          }
          return obj;
        });
    }
    return apiRequest;
  }
  angular.module('angAppTest')
  .factory('apiRequest', ['$http',apiRequest]);
})();
